# Front-End de la prueba técnica de SOSAFE

> Walter Devia

**[Ver aplicación](https://sosafe-test-front-end.walterdl.now.sh/)**

## Advertencia de rendimiento

El despliegue se encuentra en un entorno gratuito y por tanto limitado. Pueden haber retrasos en las respuestas o peticiones http perdidas.

## Ejecución local

Ejecutar `npm start` tras instalar dependencias. Notar que se requiere de dos variables de entorno con las urls de los servicios que soportan la aplicación. En local puede crear un archivo `.env` basado del archivo `.env.example` para iniciar.

### Demo

En la imagen se demuestra como se crea un evento, se filtran y se reciben notificaciones de eventos creados.

![demo](readme-assets/demo.gif)

_Un error conocido: la instancia de la nube notifica los penúltimos eventos por causas externas._

## Tecnologías

- React
- Aplicación soportada por `create-react-app`
- TypeScript 3+
- [Material UI](http://material-ui.com/)
- [styled-components](https://www.styled-components.com/)
- Pusher
- [Apollo Client](https://sosafe-test-front-end-amuck-crown.now.sh/)
- GraphQL

## Diseño de comunicaciones

![demo](readme-assets/architecture.png)

La aplicación front-end consulta las claves de uso de APIs externas del servicio [client-setup-service](https://gitlab.com/sosafe-test/client-setup-service), cuyos valores se obtienen de variables de entorno del servidor. Luego el front-end consume una API graphql del servicio [events-service](https://gitlab.com/sosafe-test/events-service) para realizar operaciones CRUD de los eventos. Por último la plataforma Pusher gestiona las notificaciones de creación de eventos lanzadas por `events-service`.

## Estándar de codificación

El proyecto usa `ESLint` junto con las extensiones para TypeScript y React. Antes de cada commit el análisis de cumplimiento del estandar se ejecuta automáticamente para asegurar que no se incluya en el repositorio código que infrinja el estandar. Esto se hace mediante la librería [husky](https://www.npmjs.com/package/husky`). Ver las reglas del estandar en el archivo `.eslintrc.json`.

## Estructura del proyecto

### Compopnentes

Algunos componentes se divien en dos módulos: módulo de estilos y módulo de lógica, nombrados respectivamente `index.ts` y `component.tsx` dentro de una carpeta con el nombre del componente, esto es:

```
<component-name>/
-- index.ts <-- styles
-- component.tsx <-- logic
```

El módulo de estilos es un componente HOC que re-exporta el módulo de lógica con estilos definidos con `styled-componentes`. Ejemplo:

```typescript
// my-component/component.tsx
import React from "react";

export default class MyComponent extends React.Component {
  // Render and logic here
}

//----

// my-component/index.ts
import styled from "styled-components";
import MyComponent from "./component";

export default styled(MyComponent)`
  display: relative;
  color: red;
`;
```

Esto permite importar al mismo tiempo un componente con lógica y estilos mediante `import MyComponent from 'path/to/my-component'`.

Los componentes que no tienen estilos personalizados no se contienen dentro de una carpeta con su nombre, simplemente se ubican en la carpeta estructural que le corresponde en un archivo `.tsx` con su nombre. Ejemplo:

```
components/
-- layout/
---- my-logic-component.tsx <-- logic
```

Algunos componentes se constituyen de otros componentes presentacionales ubicados al mismo nivel del componente que los consume, algunas veces dentro de sus propias carpetas si se constituyen de módulos de estilos.

### Declaraciones de tipos

Las declaraciones de tipos TypeScript se ubican en sus propios módulos lo más cerca a los módulos que los importa. La dedicación de un módulo completo que exporte sólo interfaces se hace para favorecer [SRC](https://en.wikipedia.org/wiki/Single_responsibility_principle).

### Código compartido

La carpeta `common` contiene declaraciones de tipo, clases de utilidad y constantes de consumo general.

## Patrones

### Componentes contenedores y presentacionales

La anidación de los componentes representa el patrón del componente contenedor constituido de varios componentnes presentacionales o compartidos, donde el contenedor gestiona la vida de un estado local y la lógca de negocio, delegando la presentacion a los componentes que lo constituyen compartiendo partes del estado local que mantiene.

### Clases de utilidad Singleton

Entre los elementos que contiene la carpeta `common` se tienen clases singletón que proveen infraestructura compartida. Los componentes contenedores hacen uso de estas clases para acceder a lógica transversal. Las clave de APIs de terceros y notificaciones push son un ejemplo de estas clases.

### Precargador

Dado que la aplicación requiere de claves de APIs de terceros se restringe la renderizacion de los componentes de lógca de negocio principales hasta que dichos valores se encuentren disponibles. El componente `Preloader` junto con la clase de utilidad `ClientSetupProvider` son usados para esto.

## Despliegue

La aplicación se despliega en [Zeit Now](https://zeit.co/) desde terminal local. Los servicios back-end que soportan el front-end también se encuentran desplegados en dicha plataforma con despliegue contínuo habilitado como [servicios serverless](https://zeit.co/docs/v2/serverless-functions/introduction/).
