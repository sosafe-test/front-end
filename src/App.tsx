import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import CssBaseLine from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/styles'
//
// Sets the MUI styles tag at the top of <head />
// so styles tags from styled components gets more specificity over MUI styles tags.
import { StylesProvider } from '@material-ui/styles'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ApolloProvider } from '@apollo/react-hooks'


// Own
// Configs
import theme from 'config/theme'
import apolloClient from 'config/apollo-client'
// Components
import Layout from 'layout'
import Preloader from 'layout/preloader'
import AddEvent from 'components/add-event'
import EventsShowcase from 'components/events-showcase'

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <StylesProvider injectFirst>
        <CssBaseLine />
        <ApolloProvider client={apolloClient}>
          <BrowserRouter>
            <Layout>
              <Preloader>
                <Switch>
                  <Route component={EventsShowcase} path="/" exact />
                  <Route component={AddEvent} path="/add-event" />
                  {/* TODO: */}
                  {/* <Route component={NotFound} /> */}
                </Switch>
              </Preloader>
            </Layout>
            <ToastContainer autoClose={4000} />
          </BrowserRouter>
        </ApolloProvider>
      </StylesProvider>
    </ThemeProvider>
  )
}

export default App
