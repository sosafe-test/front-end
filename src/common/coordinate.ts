export interface Coordinate {
  lat: number | '';
  lng: number | '';
}

export interface MarkerCoordinate extends Coordinate {
  id?: string;
  name?: string;
  onClick?: (markerCoordinate: MarkerCoordinate) => any
}

export function isValidCoordinate(coord?: Coordinate): boolean {
  return (
    !!coord && isValidCoordUnit(coord.lat) && isValidCoordUnit(coord.lng)
  )
}

export function isValidCoordUnit(coordUnit?: any): boolean {
  return !!coordUnit && typeof coordUnit === 'number' && !isNaN(coordUnit)
}
