import { Coordinate } from './coordinate'

export interface Event {
  id: string;
  name: string;
  description: string;
  coordinate: Coordinate
}
