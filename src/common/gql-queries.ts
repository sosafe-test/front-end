import gql from 'graphql-tag'

export const CREATE_EVENT = gql`
  mutation createEvent($name: String!, $coordinate: CoordinateInput!, $description: String) {
    createEvent(name: $name, description: $description, coordinate: $coordinate) {
      id
    }
  }
`

export const GET_EVENTS_BY_FILTER = gql`
  query getEventsByFilter($filter: [String]!) {
    events(keywords: $filter) {
      id
      name
      description
      coordinate {
        lat
        lng
      }
    }
  }
`
