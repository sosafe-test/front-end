export const EVENTS_SHOWCASE = `
Filtre por palabras clave utilizando el campo de texto. Los eventos se filtran si el nombre del evento contiene el texto ingresado.

En tiempo real se agregan eventos al mapa que calcen el filtro actual.

Al dar click en un marcador del mapa se presenta detalles del evento.
`

export const ADD_EVENT = `
Puede agregar rápidamente coordenadas dando click en una posición en el mapa.
`