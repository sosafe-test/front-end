import { ClientSetup } from './types'

export default class ClientSetupProvider {
  private static fetchProcess: Promise<void> | null = null
  private static fetched = false
  private static error: string | null = null
  private static _clientSetup: ClientSetup = {
    googleMapsApiKey: '',
    pusherKey: ''
  }

  public static get clientSetup(): ClientSetup {
    return Object.assign({}, this._clientSetup)
  }

  private static get validUrl(): boolean {
    return typeof process.env.REACT_APP_CLIENT_SETUP_SERVICE_ENDPOINT === 'string'
  }

  public static async loadClientSetup(options?: { force: boolean }): Promise<ClientSetup> {
    if (this.fetched && (!options || !options.force)) {
      return this.getClientSetup()
    }

    if (!this.fetchProcess) {
      this.fetchProcess = this.fetchClientSetup()
    }

    await this.fetchProcess
    this.fetched = true
    this.fetchProcess = null

    return this.getClientSetup()
  }

  private static getClientSetup(): Promise<ClientSetup> {
    if (this.error) {
      return Promise.reject(this.error)
    }

    return Promise.resolve(this.clientSetup)
  }

  private static async fetchClientSetup(): Promise<void> {
    if (!this.validUrl) {
      this.error = 'Not enough resources to get data.'
      return
    }

    return fetch(process.env.REACT_APP_CLIENT_SETUP_SERVICE_ENDPOINT as string, { method: 'GET' })
      .then((result: Response) => result.json())
      .then(data => {
        if (data && data.googleMapsApiKey) {
          this._clientSetup = {
            googleMapsApiKey: data.googleMapsApiKey,
            pusherKey: data.pusherKey
          }
        } else {
          this.error = 'Not enough resources to get data from server.'
        }
      })
      .catch(error => {
        this.error = error + '' // Converts to string implicitly
      })
  }
}