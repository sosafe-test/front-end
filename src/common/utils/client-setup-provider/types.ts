export interface ClientSetup {
  googleMapsApiKey: string;
  pusherKey: string;
}
