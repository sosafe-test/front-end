import Pusher from 'pusher-js'

// Own
import ClientSetupProvider from './client-setup-provider'
import { Event as EventModel } from 'common/event'

// Enable pusher logging - don't include this in production
if (process.env.NODE_ENV !== 'production') {
  Pusher.logToConsole = true
}

interface EventCreatedCallback {
  (event: EventModel): any
}

export default class EventsCreatedNotifier {
  private static listeners: { id: number; callback: EventCreatedCallback }[] = []
  private static pusherConnected = false
  private static listenersCounter = 0

  public static addListenerForEventCreated(callback: EventCreatedCallback): number {
    this.connectPusher()
    this.listenersCounter++

    this.listeners.push({
      id: this.listenersCounter,
      callback
    })

    return this.listenersCounter
  }

  private static async connectPusher(): Promise<void> {
    if (this.pusherConnected) {
      return
    }

    const clientSetup = await ClientSetupProvider.loadClientSetup()
    const pusher = new Pusher(clientSetup.pusherKey, {
      cluster: 'us2',
      forceTLS: true
    })
    const channel = pusher.subscribe('web-clients')
    channel.bind('event-created', (data: { event: EventModel }) => this.emitEventCreated(data))

    this.pusherConnected = true
  }

  private static emitEventCreated(data: { event: EventModel }) {
    if (data && data.event) {
      this.listeners.forEach(x => x.callback(data.event))
    }
  }

  public static removeListener(id: number) {
    const indexToRemove = this.listeners.findIndex(x => x.id === id)

    if (indexToRemove !== -1) {
      this.listeners.splice(indexToRemove, 1)
    }
  }

}
