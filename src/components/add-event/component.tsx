import React, { Component, ReactNode, ChangeEvent } from 'react'
import { Redirect } from 'react-router-dom'
import { Typography, TextField } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'
import CircularProgress from '@material-ui/core/CircularProgress'
import { toast } from 'react-toastify'

// Own
import CoordinatesSelector from './coordinates-selector'
import { Coordinate, isValidCoordinate } from 'common/coordinate'
import createEvent from './event-creator'
import { ADD_EVENT } from 'common/help-texts'
import Help from 'components/help'

interface Props {
  className?: string;
}

interface State {
  name: {
    value: string;
    isValid: boolean;
  };
  description: {
    value: string;
    isValid: boolean;
  };
  coordinate: Coordinate & {
    isValid: boolean;
  };
  saving: boolean;
  redirectToEventsShowcase?: boolean;
}

export default class AddEvent extends Component<Props, State> {
  private get canSave(): boolean {
    return this.state.name.isValid && this.state.description.isValid && this.state.coordinate.isValid
  }

  constructor(props: any) {
    super(props)
    this.state = {
      name: {
        value: '',
        isValid: false
      },
      description: {
        value: '',
        isValid: false
      },
      coordinate: {
        lat: '',
        lng: '',
        isValid: false
      },
      saving: false
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleCoordinateChange = this.handleCoordinateChange.bind(this)
    this.saveEvent = this.saveEvent.bind(this)
  }

  private handleInputChange(event: ChangeEvent<HTMLInputElement>) {
    event.persist()

    this.setState(prevState => ({
      ...prevState,
      [event.target.name]: {
        value: event.target.value,
        isValid: !!event.target.value.length
      },
    }))
  }

  private handleCoordinateChange(coordinate: Coordinate) {
    if (this.state.saving) {
      return
    }

    this.setState(() => ({
      coordinate: {
        ...coordinate,
        isValid: isValidCoordinate(coordinate)
      }
    }))
  }

  private async saveEvent() {
    if (!this.canSave) {
      return
    }

    const { name, description, coordinate } = this.state

    this.setState(() => ({ saving: true }))

    await createEvent({
      name: name.value,
      description: description.value,
      coordinate: {
        lat: coordinate.lat,
        lng: coordinate.lng
      }
    })
      .then(() => {
        toast.success('Evento creado')
        this.setState(() => ({ redirectToEventsShowcase: true }))
      })
      .catch(error => {
        toast.error(`Error creando evento. Detalles: ${error}`)
      })

    this.setState(() => ({ saving: false }))
  }

  public render(): ReactNode {
    if (this.state.redirectToEventsShowcase) {
      return <Redirect to="/" />
    }

    const { name, description, coordinate, saving } = this.state

    return (
      <div className={this.props.className}>
        <Typography variant="h2" component="p" align="center">Crear nuevo evento</Typography>
        <br />
        <Help message={ADD_EVENT} />
        <TextField
          name="name"
          label="Nombre"
          value={name.value}
          onChange={this.handleInputChange}
          error={!name.isValid}
          required
          disabled={saving}
        />
        <br/>
        <TextField
          label="Descripción"
          name="description"
          multiline
          rowsMax="4"
          value={description.value}
          onChange={this.handleInputChange}
          error={!description.isValid}
          required
          disabled={saving}
        />
        <br />
        <CoordinatesSelector
          coordinate={coordinate}
          onCoordinateChange={this.handleCoordinateChange}
        />
        {!this.canSave && <Typography className="add-event__error-msg">Por favor, ingresa nombre, descripción y coordenadas del evento.</Typography>}
        {this.renderSaveButton()}
      </div>
    )
  }

  private renderSaveButton(): ReactNode {
    const { saving } = this.state

    return (
      <div className="add-event__save-btn-container">
        <Button variant="contained" color="primary" onClick={this.saveEvent} disabled={!this.canSave || saving}>
          {saving && (
            <>
              <span>Creando evento</span>&nbsp;<CircularProgress color="secondary" size={20}/>
            </>
          )}
          {!saving && (
            <>
              <span>Crear evento</span> <SaveIcon />
            </>
          )}
        </Button>
      </div>
    )
  }
}
