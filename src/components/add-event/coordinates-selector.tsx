import React, { FC, ChangeEvent } from 'react'
import TextField from '@material-ui/core/TextField'

// Own
import Map from 'components/map'
import { Coordinate, isValidCoordUnit } from 'common/coordinate'

interface Props {
  coordinate: Coordinate;
  onCoordinateChange: (coordinate: Coordinate) => any;
  className?: string;
  disabled?: boolean;
}

const CoordinatesSelector: FC<Props> = props => {
  function handleInputChange(event: ChangeEvent<HTMLInputElement>) {
    event.persist()

    if (!props.onCoordinateChange) {
      return
    }

    const newCoordinate: Coordinate & { [key: string]: any } = {
      lat: props.coordinate.lat,
      lng: props.coordinate.lng
    }

    const newValue = parseInt(event.target.value, 10)

    newCoordinate[event.target.name] = isNaN(newValue) ? '' : newValue
    props.onCoordinateChange(newCoordinate)
  }

  function handleMapCoordinatesSelectorClick(coordinate: Coordinate) {
    if (!props.onCoordinateChange) {
      return
    }

    props.onCoordinateChange(coordinate)
  }

  return (
    <div className={props.className}>
      <TextField
        className="coords-selector__input"
        label="Latitud"
        name="lat"
        type="number"
        value={props.coordinate.lat}
        onChange={handleInputChange}
        error={!isValidCoordUnit(props.coordinate.lat)}
        required
        disabled={props.disabled}
      />
      &nbsp;
      <TextField
        className="coords-selector__input"
        label="Longitud"
        name="lng"
        type="number"
        value={props.coordinate.lng}
        onChange={handleInputChange}
        error={!isValidCoordUnit(props.coordinate.lng)}
        required
        disabled={props.disabled}
      />
      <Map markers={[props.coordinate]} onClick={handleMapCoordinatesSelectorClick} />
    </div>
  )
}

export default CoordinatesSelector
