// Own
import { CREATE_EVENT } from 'common/gql-queries'
import apolloClient from 'config/apollo-client'
import { Coordinate } from 'common/coordinate'

export default async function (payload: { name: string; coordinate: Coordinate; description?: string; }): Promise<void> {
  return apolloClient.mutate({
    mutation: CREATE_EVENT,
    variables: payload,
  })
    .then(() => { /* */ })
    .catch(error => Promise.reject(error + ''))
}
