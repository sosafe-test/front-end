import styled from 'styled-components'
import { withTheme } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core'
import AddEvent from './component'

interface Props {
  theme: Theme
}

const AddEventSC = styled(AddEvent)`
text-align: center;

.add-event__error-msg {
  margin: ${(props: Props) => props.theme.spacing(2)}px 0;
}

.add-event__save-btn-container {
  margin-top: ${(props: Props) => props.theme.spacing(2)}px;
}
`

export default withTheme(AddEventSC)
