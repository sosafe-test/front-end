// Own
import { GET_EVENTS_BY_FILTER } from 'common/gql-queries'
import apolloClient from 'config/apollo-client'
import { Event as EventModel } from 'common/event'

export default async function (filter?: string[]): Promise<EventModel[]> {
  return apolloClient.query({
    query: GET_EVENTS_BY_FILTER,
    variables: { filter: filter || [] },
    fetchPolicy: 'network-only'
  })
    .then(result => result.data.events)
    .catch(error => Promise.reject(error + ''))
}
