// Own
import { Event as EventModel } from 'common/event'

export default function (keywords: string[] | null, event: EventModel): boolean {
  if (!Array.isArray(keywords) || !keywords.length) {
    return true
  }

  return !!keywords.filter(x => event.name.includes(x)).length
}
