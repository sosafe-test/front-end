import React, { Component, ReactElement, createRef, Fragment, KeyboardEvent } from 'react'
import Chip from '@material-ui/core/Chip'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'

interface Props {
  className?: string;
  onFilterChange: (keywords: string[]) => any
}

interface State {
  keywords: string[];
}

// Persist keywords filter
let keywordsBackup: string[] = []

export default class EventsShowcaseFilter extends Component<Props, State> {
  private keywordInputRef: any

  private get thereAreKeywords(): boolean {
    return !!(this.state.keywords && this.state.keywords.length)
  }

  constructor(props: any) {
    super(props)
    this.state = {
      keywords: keywordsBackup.slice(0)
    }
    this.handleDeleteKeyword = this.handleDeleteKeyword.bind(this)
    this.handleAddKeyword = this.handleAddKeyword.bind(this)
    this.handleKeyUpKeywordInput = this.handleKeyUpKeywordInput.bind(this)
    this.keywordInputRef = createRef()
  }

  public componentDidMount() {
    // The component is initialized with a backup of keywords
    // This must be notified to parent component
    // Since it's still rendering I must to wait the next tick.
    setTimeout(() => {
      this.props.onFilterChange(this.state.keywords.slice(0))
    }, 0)
  }

  private handleDeleteKeyword(keywordToDelete: string) {
    const keywordsCopy = this.state.keywords.slice(0)
    keywordsCopy.splice(keywordsCopy.indexOf(keywordToDelete), 1)
    this.updateKeywords(keywordsCopy)
  }

  private handleAddKeyword() {
    const input: HTMLInputElement = this.keywordInputRef && this.keywordInputRef.current

    if (!input) {
      return
    }

    const value = input.value.trim()

    if (!value.length || this.state.keywords.includes(value)) {
      return
    }

    const keywordsCopy = this.state.keywords.slice(0)
    keywordsCopy.push(value)
    this.updateKeywords(keywordsCopy)
    this.clearKeywordInput()
  }

  private clearKeywordInput() {
    const input: HTMLInputElement = this.keywordInputRef && this.keywordInputRef.current

    if (!input) {
      return
    }

    input.value = ''
  }

  private updateKeywords(newKeywords: string[]) {
    this.setState(() => ({
      keywords: newKeywords
    }), () => {
      this.props.onFilterChange(this.state.keywords.slice(0))
    })
  }

  private handleKeyUpKeywordInput(ev: KeyboardEvent<HTMLDivElement>) {
    if (ev.keyCode === 13) {
      // It's enter
      this.handleAddKeyword()
    }
  }

  public render(): ReactElement {
    return (
      <div className={this.props.className}>
        <div className="events-showcase-filter__input-container">
          <TextField
            label="Ingresar keyword"
            type="text"
            inputRef={this.keywordInputRef}
            onKeyUp={this.handleKeyUpKeywordInput}
          />
          <Button variant="outlined" color="primary" onClick={this.handleAddKeyword}>
            Agregar filtro&nbsp;<AddIcon />
          </Button>
        </div>
        {this.thereAreKeywords && (
          <>
            <p>Filtrando por:</p>
            {this.state.keywords.map((x, index) => (
              <Fragment key={index}>
                <Chip label={x} onDelete={this.handleDeleteKeyword} color="primary" variant="outlined" />&nbsp;
              </Fragment>
            ))}
          </>
        )}
        {!this.thereAreKeywords && (
          <span>Sin palabras clave para filtrar</span>
        )}
      </div>
    )
  }

  public componentWillUnmount() {
    keywordsBackup = this.state.keywords
  }
}
