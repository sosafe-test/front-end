import styled from 'styled-components'
import { withTheme } from '@material-ui/styles'
import { Theme } from '@material-ui/core'

// OWn
import EventsShowcaseFilter from './component'

interface Props {
  theme: Theme
}

const EventsShowcaseFilterSC = styled(EventsShowcaseFilter)`
  text-align: center;

  .events-showcase-filter__input-container {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: ${(props: Props) => props.theme.spacing(1)}px 0;
  }
`

export default withTheme(EventsShowcaseFilterSC)
