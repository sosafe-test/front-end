import React, { Component, ReactElement } from 'react'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import RefreshIcon from '@material-ui/icons/Refresh'
import { toast } from 'react-toastify'

// Own
import getEventsByFilter from './events-by-filter-provider'
import { Event } from 'common/event'
import Map from 'components/map'
import { MarkerCoordinate } from 'common/coordinate'
import Filter from './filter'
import checkIfEventMatchFilter from './filter/check-if-event-match-filter'
import EventsCreatedNotifier from 'common/utils/events-created-notifier'
import { Event as EventModel } from 'common/event'
import { EVENTS_SHOWCASE } from 'common/help-texts'
import Help from 'components/help'

interface Props {
  className?: string;
}

interface State {
  loading: boolean
  events: Event[]
  error?: string
}

export default class EventsShowcase extends Component<Props, State> {
  private eventCreatedListenerId: number | null = null
  private filter: string[] | null = null

  constructor(props: any) {
    super(props)
    this.state = { loading: true, events: [] }
    this.getEvents = this.getEvents.bind(this)
    this.showEvent = this.showEvent.bind(this)
  }

  public componentDidMount() {
    this.getEvents()
    this.eventCreatedListenerId = EventsCreatedNotifier.addListenerForEventCreated(this.handleEventCreated.bind(this))
  }

  private handleEventCreated(event: EventModel) {
    const itMatch = checkIfEventMatchFilter(this.filter, event)

    if (itMatch) {
      const eventsCopy = this.state.events.slice(0)
      eventsCopy.push(event)
      this.setState(() => ({
        events: eventsCopy
      }), () => {
        toast.info(`Se ha creado nuevo evento: ${event.name}`)
      })
    }
  }

  private async getEvents(filter?: string[]) {
    this.filter = filter || []
    this.setState(() => ({ loading: true, error: undefined }))

    await getEventsByFilter(filter)
    .then(events => {
      this.setState(() => ({
        events
      }))
    })
    .catch(error => this.setState({ error }))

    this.setState(() => ({ loading: false }))
  }

  private showEvent(markerCoord: MarkerCoordinate) {
    const event = this.state.events.find(x => x.id === markerCoord.id)

    if (event) {
      alert(`
      name: ${event.name}
      description: ${event.description}
      coordinate: {lat: ${event.coordinate.lat}, lng: ${event.coordinate.lng}}
      `)
    }
  }

  public render(): ReactElement {
    return (
      <Box className={this.props.className}>
        <Typography variant="h2" component="p" align="center">Eventos registrados</Typography>
        <br/>
        <Help message={EVENTS_SHOWCASE} />
        {this.renderLoadingIndicator()}
        {this.renderErrorIndicator()}
        <Filter onFilterChange={this.getEvents} />
        {this.renderMap()}
      </Box>
    )
  }

  private renderLoadingIndicator(): ReactElement | null {
    if (!this.state.loading) {
      return null
    }

    return (
      <Typography component="div" align="center">
        <span>Cargando eventos</span>&nbsp;<CircularProgress color="secondary" size={20}/>
      </Typography>
    )
  }

  private renderErrorIndicator(): ReactElement | null {
    if (!this.state.error) {
      return null
    }

    return (
      <Typography component="p" align="center">
        <span>Error al cargar eventos. Detalles: {this.state.error}</span>
        <br />
        <Button variant="contained" color="primary" onClick={() => this.getEvents()}>
          Recargar <RefreshIcon />
        </Button>
      </Typography>
    )
  }

  private renderMap(): ReactElement {
    const markers: MarkerCoordinate[] = this.state.events.map(x => ({
      lat: x.coordinate.lat,
      lng: x.coordinate.lng,
      name: x.name,
      id: x.id,
      onClick: this.showEvent
    }))

    return (
      <Map markers={markers} onClick={this.showEvent} />
    )
  }

  public componentWillUnmount() {
    EventsCreatedNotifier.removeListener(this.eventCreatedListenerId as number)
  }
}
