import React, { FC } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import HelpOutlineIcon from '@material-ui/icons/HelpOutline'

const Help: FC<{ message: string }> = props => (
  <Card>
    <CardContent>
      <HelpOutlineIcon style={{ verticalAlign: 'middle' }}/>
      &nbsp;
      {props.message}
    </CardContent>
  </Card>
)

export default Help
