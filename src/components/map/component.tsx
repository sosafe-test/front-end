import React, { FC, ReactElement } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'

// Own
import ClientSetupProvider from 'common/utils/client-setup-provider'
import { MarkerCoordinate, isValidCoordinate, Coordinate } from 'common/coordinate'

interface RawMapProps {
  markers?: MarkerCoordinate[];
  onClick?: (coordinate: Coordinate) => any;
}

const RawMap: FC<RawMapProps> = props => {
  function handleOnClick(ev: any) {
    if (!props.onClick) {
      return
    }

    props.onClick({
      lat: ev.latLng.lat(),
      lng: ev.latLng.lng()
    })
  }

  function renderMarkers(): ReactElement[] {
    if (!props.markers || !props.markers.length) {
      return []
    }

    return props.markers
      .filter(x => isValidCoordinate(x))
      .map((x, index) => (
        <Marker
          key={index}
          position={{ lat: x.lat as number, lng: x.lng as number }}
          label={(x.name || '') as any}
          onClick={() => x.onClick && x.onClick(x)}
        />
      ))
  }

  return (
    <GoogleMap
      defaultZoom={8}
      defaultCenter={{ lat: -34.397, lng: 150.644 }}
      onClick={handleOnClick}
    >
      {renderMarkers()}
    </GoogleMap>
  )
}

const MapWithDomInstances = withGoogleMap(RawMap)
const MapWithScriptJS = withScriptjs(MapWithDomInstances)

interface MapCoordinatesSelectorProps extends RawMapProps {
  height?: string
  className?: string;
}

const MapCoordinatesSelector: FC<MapCoordinatesSelectorProps> = props => {
  const { height = 400 } = props

  return (
    <div className={props.className}>
      <MapWithScriptJS
        googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${ClientSetupProvider.clientSetup.googleMapsApiKey}&v=3.exp&libraries=geometry,drawing,places`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `${height}px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        onClick={props.onClick}
        markers={props.markers}
      />
    </div>
  )
}

export default MapCoordinatesSelector
