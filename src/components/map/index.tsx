import styled from 'styled-components'
import { withTheme } from '@material-ui/styles'
import { Theme } from '@material-ui/core'

// Own
import Map from './component'

interface Props {
  theme: Theme
}

const MapSC = styled(Map)`
  margin-top: ${(props: Props) => props.theme.spacing(2)}px;
  margin-bottom: ${(props: Props) => props.theme.spacing(2)}px;
`

export default withTheme(MapSC)
