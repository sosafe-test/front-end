import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles'
import blueGrey from '@material-ui/core/colors/blueGrey'
import indigo from '@material-ui/core/colors/indigo'

const theme = createMuiTheme({
  palette: {
    primary: blueGrey,
    secondary: indigo,
  },
})

const themeWithResponsiveFontSize = responsiveFontSizes(theme)

export default themeWithResponsiveFontSize