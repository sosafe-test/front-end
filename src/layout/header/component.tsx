import React, { FC } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import AssistantIcon from '@material-ui/icons/Assistant'
import AddAlertIcon from '@material-ui/icons/AddAlert'
import { Link } from 'react-router-dom'

interface Props {
  className?: string;
}

const linkStyle = {
  textDecorationLine: 'none'
}

const Header: FC<Props> = props => (
  <AppBar className={props.className}>
    <div className="header__links-container">
      <Link to="/" style={linkStyle}>
        <Button>
          <AssistantIcon /> Eventos
        </Button>
      </Link>
      <Link to="/add-event" style={linkStyle}>
        <Button>
          <AddAlertIcon /> Agregar evento
        </Button>
      </Link>
    </div>
    <p className="header__author">@WalterDevia</p>
  </AppBar>
)

export default Header
