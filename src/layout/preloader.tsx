import React, { Component, ReactElement } from 'react'
import { Typography } from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import RefreshIcon from '@material-ui/icons/Refresh'

// Own
import ClientSetupProvider from 'common/utils/client-setup-provider'
import { ClientSetup } from 'common/utils/client-setup-provider/types'

interface Props {
  className?: string
}

interface State {
  clientSetup: ClientSetup | null;
  loading: boolean;
  error: string | null;
}

export default class Preloader extends Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = { clientSetup: null, loading: true, error: null }
    this.getClientSetup = this.getClientSetup.bind(this)
  }

  public componentDidMount() {
    this.getClientSetup()
  }

  private getClientSetup() {
    this.setState(() => ({ loading: true, error: null }))

    ClientSetupProvider.loadClientSetup({ force: true })
      .then(clientSetup => {
        this.setState(prevState => ({
          ...prevState,
          clientSetup,
          error: null
        }))
      })
      .catch(error => this.setState({ error }))
      .then(() => this.setState(() => ({ loading: false })))
  }

  public render(): ReactElement {
    const { loading, error } = this.state

    if (loading || error) {
      return (
        <Typography align="center" component="div">
          {loading && (
            <>
              <span>Cargando datos iniciales</span>&nbsp;<CircularProgress color="secondary" size={20}/>
            </>
          )}
          {!loading && error && (
            <>
              <span>Error cargando datos iniciales. Detalles: {error}</span>
              <br />
              <Button variant="contained" color="primary" onClick={this.getClientSetup}>
                Recargar <RefreshIcon />
              </Button>
            </>
          )}
        </Typography>
      )
    }

    return <>{this.props.children}</>
  }
}
